# Amdatu.org Website

## Introduction

This is the source repository for the [amdatu.org](https://amdatu.org) website.
It is generated with the [Hugo static website generator](https://gohugo.io/) and hosted
in a [Docker](https://www.docker.com/) container that is managed by the
[Luminis Cloud RTI](https://www.luminis.eu/what-we-do/technology-platforms/cloud-rti/).

## Authoring

Authoring content can be done using any straightforward text editor. Hugo has a preference
for simple mark-down formatted ASCII text. In order to test-drive the site, either a local Hugo
installation will work or a Docker infrastructure. To be able to use the latter, a Dockerfile
is part of this repository.

## CI / CD

After all authoring is finished and all changes are committed, an automatic build will be
triggered using the CI /CD infrastructure in the
[Luminis Cloud infrastructure](https://www.luminis.eu/what-we-do/technology-platforms/cloud-rti/).
This will automatically build and push a new docker image with the `staging` tag.
Once the changes have been evaluated and approved, an additional, manual build step
has to be executed in order to update the [amdatu.org](https://amdatu.org) website.
All images can be found in the [Docker hub](https://hub.docker.com/r/amdatu/amdatu-website/).

## Issues

Please report issues on [Amdatu Website Jira](https://amdatu.atlassian.net/projects/WEBSITE/issues).
