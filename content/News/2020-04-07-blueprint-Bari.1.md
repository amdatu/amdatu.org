---
date: "2020-04-09"
title: "Amdatu Blueprint Bari.1 was released!"
---

By issuing a new minor release of the Amdatu Blueprint, “Bari.1” we are making a number of important improvements available. This minor release combines the improvements of a number of underlying and related projects.

**Amdatu Security**: Improved token and password handling and added some important fixes to improve the quality and reliability (AMDATUSEC-64 and AMDATUSEC-66).

**Amdatu Herding**: Improved the task manager using Apache Helix to better support larger clustered configurations and overall quality (AMDATUHERD-10).

**Amdatu Modular-UI**: Improved layout of static resources and other improvements (MODUI-78).

**Amdatu Idea**: Update to support IntelliJ 2019.3.

Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.
