---
date: "2020-09-16"
title: "Amdatu Blueprint Corporal.3 was released!"
---

By issuing a new release of the Amdatu Blueprint, “Corporal.3” we are making a number of important improvements available. This minor release combines the improvements of a number of underlying and related projects.

**Amdatu Validator**:
 
 * Update to Hibernate Validator 6.1.5. [VALIDATOR-3](https://amdatu.atlassian.net/browse/VALIDATOR-3)
 