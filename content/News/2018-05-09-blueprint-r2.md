---
date: "2018-05-02"
title: "Amdatu Blueprint r2 has been released!"
---

With Amdatu blueprint we provide a standardized workspace that provides templates to get quickly you up and running with building modular applications using Amdatu components. Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.
This release contains a fix for the broken Eclipse setup.