---
date: "2021-08-06"
title: "Amdatu Blueprint Dania released!"
---

By issuing a new release of the Amdatu Blueprint, “Dania” we are making a number of important improvements available. This major release combines the improvements of a number of underlying and related projects.

**Amdatu Mongo**: Update Mongo driver to 4.1

**Amdatu Herding**: Upgrade Helix to version 1.0.1 to solve ConcurrentModificationException (AMDATUHERD-16)

**Amdatu Web**: Using JAX-RS StreamingOutput to create chunked response doesn't work (AMDATUWEB-82)

**Amdatu Security**:

- Call to /logout always throws an IllegalArgumentException (AMDATUSEC-76)
- Extend the Action interface with an applies(Action) method to allow authorization policies to match a set of actions with a single policy. (AMDATUSEC-75)
- Unclear exception message from LocalIdProvider when credentials not found (AMDATUSEC-74)
- Make the token available as request attribute after successful login (AMDATUSEC-73)
- Support for Same-site cookie attribute (AMDATUSEC-67)

**Amdatu Idea**: Updated to support IntelliJ 2021.1 and Bnd 5.3.0


Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.