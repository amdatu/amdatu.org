---
date: "2019-10-03"
title: "Amdatu Blueprint Bari was released!"
---

Following the blueprint naming scheme, the new blueprint release named "Bari" was released today. Along with the release, also a number of underlying and related projects were updated.

**Amdatu Security**: Updated the OpenID connector with some important fixes to improve the quality and reliability (AMDATUSEC-60 and AMDATUSEC-65).

**Amdatu Herding**: Improved the locking mechanisms to better support larger clustered configurations and optimised to logging implementation (AMDATUHERD-4, AMDATUHERD-5 and AMDATUHERD-6).

**Amdaty Idea**: Improved the test-support by offering test features in the IDE and identifying sources of itest projects as tests.

And last but not least we have updated the Amdatu build environments to use BND 4.2 and also a number of 3rd-party dependencies to stay current.

Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.
