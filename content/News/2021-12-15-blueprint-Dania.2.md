---
date: "2021-12-15"
title: "Amdatu Blueprint Dania.2 released!"
---

By issuing a new release of the Amdatu Blueprint, “Dania.2” we are making sure projects using Amdatu Blueprint remain protected against the 0-day Log4J issue ([CVE-2021-44228](https://www.lunasec.io/docs/blog/log4j-zero-day/)). 



Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.