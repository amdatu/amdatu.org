---
date: "2018-06-19"
title: "Amdatu Blueprint r3 has been released!"
---

With Amdatu blueprint we provide a standardized workspace that provides templates to get quickly you up and running with building modular applications using Amdatu components.<br />
In this release we've added support for creating Docker images for your application and the security feature has been updated to use the latest Amdatu Security version. 

Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.