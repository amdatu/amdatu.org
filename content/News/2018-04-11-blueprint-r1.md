---
date: "2018-04-11"
title: "Amdatu Blueprint r1 has been released!"
---


With Amdatu blueprint we provide a standardized workspace that provides templates to get quickly you up and running with building modular applications using Amdatu components. Check out the blueprint [documentation](/components/amdatu-blueprint/r1) to get started quickly.
