---
date: "2020-06-12"
title: "Amdatu Blueprint Corporal.1 was released!"
---

By issuing a new release of the Amdatu Blueprint, “Corporal.1” we corrected the referece to the Amdatu Web and Amdatu Security repository. 

Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.
