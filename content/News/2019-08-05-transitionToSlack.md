---
date: "2019-08-05"
title: "Amdatu transitions from mailing lists to Slack"
---

Ever since the beginning of the project, the Amdatu community used mailing lists for communication and collaboration. With the introduction of a project Wiki [Amdatu wiki](https://amdatu.atlassian.net/wiki/spaces/AMDATUDEV/overview) a different type of collaboration was introduced which resulted in a negligible level of traffic on the mailing lists.
Therefore we have decided to transition away from mailing lists and only use Slack for direct commmunication.
For that purpose we will use the existing [Amdatu Slack channel](https://amdatu.slack.com).
