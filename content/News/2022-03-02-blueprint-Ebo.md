---
date: "2022-03-23"
title: "Amdatu Blueprint Ebo released!"
---

By issuing a new release of the Amdatu Blueprint, “Ebo“ we are making a number of important improvements available. This major release combines the improvements of a number of underlying and related projects.

**All projects**: Build and run with and for Java 11, all projects have been upgraded to Java 11. 

**Amdatu Web**:  

- Using JAX-RS StreamingOutput to create chunked response doesn't work (AMDATUWEB-82)
- Export resteasy spi packages from the resteasy bundle (AMDATUWEB-83)

**Amdatu Security**:  

- Make subjectRoles ENTITY_TYPE constant available in API (AMDATUSEC-71)
- ConcurrentModificationException when running with Java 17 (AMDATUSEC-78)
- Token remains invalid after renewal causing lots of renewals (AMDATUSEC-64)
 

**Amdatu Mongo**: Update MongoDB client driver to 4.3.3 (AMDMONGODB-14)

**Amdatu Herding**: Make herding work with Java 14 and above (AMDATUHERD-17)

**Amdaty Idea**: 

- Make it easier to find the origin of errors reported by bnd that don't have a location (AMDATUIDEA-39)
- Add support for preview language level (AMDATUIDEA-38)


Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.
