---
date: "2020-05-22"
title: "Amdatu Blueprint Corporal was released!"
---

By issuing a new release of the Amdatu Blueprint, “Corporal” we are making a number of important improvements available. This major release combines the improvements of a number of underlying and related projects.

**Amdatu Security**: Create multiple cookies when the token size becomes larger then 4k (AMDATUSEC-68).

**Amdatu Idea**: Update to support IntelliJ 2020.1.

Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.
