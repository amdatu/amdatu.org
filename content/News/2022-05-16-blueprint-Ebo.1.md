---
date: "2022-05-16"
title: "Amdatu Blueprint Ebo.1 released!"
---

By issuing a new release of the Amdatu Blueprint, Ebo.1” we fixed an issue with the OpenId provider in Amdatu Security (AMDATUSEC-80). 


Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.
