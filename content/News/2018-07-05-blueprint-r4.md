---
date: "2018-07-05"
title: "Amdatu Blueprint r4 has been released!"
---

With Amdatu blueprint we provide a standardized workspace that provides templates to get quickly you up and running with building modular applications using Amdatu components.<br />
The Gradle script for docker support that was missing in the r3 release has been added.

Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.