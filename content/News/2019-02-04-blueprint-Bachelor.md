---
date: "2019-02-04"
title: "Amdatu Blueprint Bachelor has been released!"
---

Bachelor is the first release of Amdatu Blueprint that has a proper name, instead of r1, r2 we now have a named releases. 
Besides the name there are obviously other changes worth mentioning. 

We moved the Gradle script support for creating Docker images into a separate Gradle plugin. This greatly simplifies the 
setup of creating Docker images from your build and makes migrating to newer versions of Amdatu Blueprint easier.

The Gradle build has been updated to use Bnd 4.1.0.
To make sure the IDE and Gradle build produce the same results use the [Amdatu IDEA IntelliJ plugin]( https://plugins.jetbrains.com/plugin/10639-amdatu) version 2.1.0 or Bndtools 4.1.0

We have added additional meta data into our Amdatu Bluprint features to support the Amdatu InteliJ plugin, this makes that we now have auto completion for feature names in IntelliJ.

An expirimental feature has been added to support Java 11.
This feature has been added as a fist step to supporting Java 11 and not all components have been tested so it's not recommended for production yet.

**Amdatu Web**: Comes with new default JAX-RS ExceptionMapper that generates an exception code that's logged with the exeption. 
This makes it easy to find exceptions reported by a user in the log.   

**Amdatu Mongo:** Updated to the Mongo driver to version 3.9.1.

**Amdatu Security:** Several bugfixes.

And last but not least we have updated all 3rd-party dependencies to stay current.

Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.