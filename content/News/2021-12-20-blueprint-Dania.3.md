---
date: "2021-12-20"
title: "Amdatu Blueprint Dania.3 released!"
---

By issuing a new release of the Amdatu Blueprint, “Dania.1” we are making sure projects using Amdatu Blueprint are protected against the 0-day Log4J issue ([CVE-2021-45105](https://nvd.nist.gov/vuln/detail/CVE-2021-45105) )



Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.