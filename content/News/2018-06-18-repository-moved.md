---
date: "2018-06-18"
title: "Amdatu repository has moved"
---

The Amdatu repositories have been moved to S3. 
The repositories in the [git repo](https://bitbucket.org/amdatu/amdatu-repository)  won't be updated anymore it's contents remain available on https://repository.amdatu.org/ as they used to be. 
New Amdatu component releases will be published to S3 directly and links to the latest release of each component are available on the [Components](/components) page.