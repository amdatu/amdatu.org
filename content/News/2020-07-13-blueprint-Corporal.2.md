---
date: "2020-07-12"
title: "Amdatu Blueprint Corporal.2 was released!"
---

By issuing a new release of the Amdatu Blueprint, “Corporal.2” we are making a number of important improvements available. This minor release combines the improvements of a number of underlying and related projects.

**Amdatu Security**:
 
 * Add support for configuring the `SameSite` cookie attribite value. [AMDATUSEC-67](https://amdatu.atlassian.net/browse/AMDATUSEC-67)
 
 * Resolved unpredictability problem when there are multple EntityProviders for the same entity [AMDATUSEC-69](https://amdatu.atlassian.net/browse/AMDATUSEC-69)
 
 * Introduced role RoleProvider interface used to get roles available for a JAX-RS application (this replaces the `SubjectRoleEntityProvider`) [AMDATUSEC-70](https://amdatu.atlassian.net/browse/AMDATUSEC-70)

**Amdatu Web**: Added the JAX-RS application name to the `RequestContext` used by the  `JaxRsRequestInterceptor`

Check out the blueprint [documentation](/components/amdatu-blueprint/) to get started quickly.
