---
date: 2018-02-05T12:43:28+01:00
---

What is Amdatu about?
---------------------
Amdatu is an open source project, which is aimed at fostering a community that is involved in  the development of scalable and robust applications using dynamic modular services architectures. It is designed for distributed systems ranging from high traffic cloud systems up to massive IoT solutions. In order to do so, it is designed using an architectural style that is based on the following founding principles:

* Adhere to standards that promote speed of development and open innovation.
* Abstract from underlying infrastructure to prevent restrictive vendor lock-in.
* Assume the fallacies of distributed computing as a given.
* Organize according to accepted Open Source principles to grow a community.


Background of the Amdatu Foundation
-----------------------------------
The underlying mental framework of Amdatu relates to the rapidly decreasing relevance of a number of (20th Century) concepts and business strategies; this is especially true of strategies based on the monopolization of ideas, the ownership of information and commercial lock-in strategies.
As ideas and information are exchanged with increasing speed and traditional barriers for (remote)
collaboration are removed, the relevance of these concepts is being supplanted
by strategies focused on rapid, collaborative evolutionary innovation.
These concepts emphasize the increasing relevance of transparency, cooperation, dynamism and speed.

This philosophy was embraced by a number of Dutch organizations and resulted in the formation of the Amdatu Foundation. The goal of this foundation is to establish such a community aimed at the development of modular cloud applications.
