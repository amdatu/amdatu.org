---
date: 2018-02-05T12:19:54+01:00
---
Amdatu blueprint
----------------
The [Amdatu Blueprint](amdatu-blueprint) provides a ready-to-go workspace to start application development. The blueprint comes with the following pre-packaged Amdatu components.

Amdatu components
-----------------

The Amdatu components included in Amdatu blueprint are listed below. They can be used separately in your OSGi project, but it's highly recommended to
use  to setup your development environment and project. For all of the other Amdatu projects please checkout the source repository on [BitBucket](https://bitbucket.org/amdatu/).

##### Amdatu Web
Amdatu Web offers the JAX-RS API to easily implement RESTful web services. JAX-RS is a Java EE standard that provides support to create RESTful web services with use of annotations. This makes adding a RESTful web service in Amdatu as simple as registering an annotated class as an OSGi service. It will automatically be picked up by the framework and made accessible as REST endpoint.

| | |
| ------- | ------ |
|Documentation| [Amdatu Web documentation](amdatu-web) |
|Issue tracker| https://amdatu.atlassian.net/projects/AMDATUWEB/issues |
|Release repository| https://repository.amdatu.org/amdatu-web/latest.xml |

___

##### Amdatu Security
Amdatu Security provides a solution for authentication and authorisation of (web-based) applications. 

| | |
| ------- | ------ |
|Documentation| [Amdatu Security documentation](amdatu-security) |
|Issue tracker| https://amdatu.atlassian.net/projects/AMDATUSEC/issues|
|Release repository| https://repository.amdatu.org/amdatu-security/latest.xml|

___

##### Amdatu Configurator

Amdatu Configurator provides a simple way of provisioning configuration files to your OSGi application, allowing you to use the same set of configuration files to run your OSGi application locally as you would need for deploying your application using Apache ACE or DeploymentAdmin.

| | |
| ------- | ------ |
|Documentation| [Amdatu Configurator documentation](amdatu-configurator) |
|Issue tracker| https://amdatu.atlassian.net/projects/AMDATUCONF/issues|
|Release repository| https://repository.amdatu.org/amdatu-configurator/latest.xml|
___

##### Amdatu Testing
The Amdatu Testing project provides static utilities to simplify OSGi service integration tests. 

| | |
| ------- | ------ |
|Documentation| [Amdatu Testing documentation](amdatu-testing) |
|Issue tracker| https://amdatu.atlassian.net/projects/AMDATUTEST/issues|
|Release repository| https://repository.amdatu.org/amdatu-testing/latest.xml|

___

##### Amdatu MongoDB
The MongoDB component makes it easy to use MongoDB from OSGi. The component is a Managed Service Factory that enables you to configure MongoDB using Configuration Admin.

| | |
| ------- | ------ |
|Documentation| [Amdatu MongoDB documentation](amdatu-mongodb) |
|Issue tracker| https://amdatu.atlassian.net/projects/AMDMONGODB/issues|
|Release repository| https://repository.amdatu.org/amdatu-mongodb/latest.xml|

___

##### Amdatu Herding
The MongoDB component makes it easy to use MongoDB from OSGi. The component is a Managed Service Factory that enables you to configure MongoDB using Configuration Admin.

| | |
| ------- | ------ |
|Documentation| [Amdatu MongoDB documentation](amdatu-mongodb) |
|Issue tracker| https://amdatu.atlassian.net/projects/AMDMONGODB/issues|
|Release repository| https://repository.amdatu.org/amdatu-mongodb/latest.xml|

___

##### Amdatu Blobstores
Blob Stores are used to store (large) files in the cloud. Amdatu Blob Stores is based on the jclouds library that supports Blob Stores from several cloud providers such as Amazon S3 and Microsoft Azure. The Blob Stores component makes the jclouds API available as OSGi service using a Managed Service Factory. 

| | |
| ------- | ------ |
|Documentation| [Amdatu Blobstores documentation](amdatu-blobstores) |
|Issue tracker| https://amdatu.atlassian.net/projects/AMDATUSTORAGE/issues|
|Release repository|https://repository.amdatu.org/amdatu-blobstores/latest.xml|

___

##### Amdatu Email
Amdatu Email provides a service abstraction for sending email. Currently, it provides implementations to send emails through Amazon’s Simple Email Service, through a configured SMTP server, as well as a mock implementation for use in test scenario’s.

| | |
| ------- | ------ |
|Documentation| [Amdatu Email documentation](amdatu-email) |
|Issue tracker| https://amdatu.atlassian.net/projects/AMDATUMAIL/issues|
|Release repository| https://repository.amdatu.org/amdatu-email/latest.xml|

___

##### Amdatu Validator
The Validator component brings Bean Validation to OSGi.

| | |
| ------- | ------ |
|Documentation| [Amdatu Validator documentation](amdatu-validator) |
|Issue tracker|https://amdatu.atlassian.net/projects/VALIDATOR/issues |
|Release repository|https://repository.amdatu.org/amdatu-validator/latest.xml|

___

##### Amdatu Scheduling
The Scheduling component makes it easy to schedule jobs that are executed at configured intervals or dates. The component is based on the Quartz framework, and adds a whiteboard style approach to registering jobs.

| | |
| ------- | ------ |
|Documentation| [Amdatu Scheduling documentation](amdatu-scheduling) |
|Issue tracker|https://amdatu.atlassian.net/projects/AMDATUSCH/issues|
|Release repository|https://repository.amdatu.org/amdatu-scheduling/r7/repo/index.xml.gz|

____

##### Amdatu Template
The Amdatu Template project is a service abstraction for template processing engines. The service interface abstracts away from the specific implementations of such processors.

| | |
| ------- | ------ |
|Documentation| [Amdatu Template documentation](amdatu-template) |
|Issue tracker|https://amdatu.atlassian.net/projects/AMDATUTEMP/issues|
|Release repository|https://repository.amdatu.org/amdatu-template/r3/repo/index.xml.gz|
