---
date: 2018-02-05T12:20:09+01:00
---

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@graph" :
  [{
    "@type":"Course",
    "name": "Modular Development with Java and OSGi",
    "description": "Modular Development with Java and OSGi",
    "url" : "https://academy.luminis.eu/en/onderwerpen/osgi-en/",
    "provider": {
      "@type": "Organization",
      "name": "Luminis Academy",
      "sameAs": "https://academy.luminis.eu/"
    }
  },
  { "@type": "Course",
    "name": "Designing Modular Software Systems",
    "description": "Designing Modular Software Systems",
    "provider": {
      "@type": "Organization",
      "name": "Luminis Academy",
      "sameAs": "https://academy.luminis.eu/"
    }
  },
  { "@type": "Course",
    "name": "Developing with the Amdatu Blueprint",
    "description": "Developing with the Amdatu Blueprint",
    "provider": {
      "@type": "Organization",
      "name": "Luminis Academy",
      "sameAs": "https://academy.luminis.eu/"
    }
  }

  ]
}
</script>

Getting started with modular development?
-----------------------------------------
The design and implementation of software systems using modular architectures is not a simple task.
To promote the adoption of these architectures, we advocate the training related to this topic.
Luminis, one of the founding partners of the Amdatu Foundation and forerunners in the field
of modular software development, has been educating and mentoring people in this area.

As part of the Luminis Academy, the following courses are available:

* Modular Development with Java and OSGi
* Designing Modular Software Systems
* Developing with the Amdatu Blueprint

Information about these courses can be requested at <academy@luminis.eu>.
