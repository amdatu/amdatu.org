---
date: 2018-02-05T12:19:54+01:00
---

Community
-----------------
#### Source code
The Amdatu source repositories are hosted here: https://bitbucket.org/amdatu/.

#### Wiki
The [wiki](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/AMDATU+Development+wiki) provides additional information, design documentation and in general is a great place to collaborate on designs and documents that are in progress.

#### Issue tracker
The [issue tracker](https://amdatu.atlassian.net/secure/) holds bugs and feature requests for all the components in Amdatu. It is also a great place to look for smaller and bigger things to work on and contribute.